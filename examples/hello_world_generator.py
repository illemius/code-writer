import time

from codewriter.blocks import *
from codewriter.source import Source


def main():
    source = Source('example_out/hello_world.py', add_header=True)

    # Create class
    main_class = Class('App')
    # Add class to sources
    source.body.append(main_class)

    # Add global imports
    source.add_import('typing')
    source.add_import('sys')

    # Application.__init__(argv=typing.List[str])
    init_main_class = Method('__init__', args=['argv'], type_annotations={'argv': typing.List[str]})
    init_main_class.docstring.add_line('Init application')
    init_main_class.docstring.add_param('argv', 'List of arguments with which the script is launched', list)
    init_main_class.append(Expression('self.argv').assign('argv'))
    main_class.append(init_main_class)

    # Application.get_name()
    get_name = Method('get_name')
    get_name.add_return(Ternary('self.argv', '" ".join(self.argv)', '"world"'))
    main_class.append(get_name)

    # Application.execute() -> int
    execute_application = Method('execute', type_annotations={RETURN_ANNOTATION: int})
    execute_application.docstring.add_line('You can execute application by calling that method')
    execute_application.docstring.add_return('Exit code', int)
    execute_application.append(Expression('name').assign(get_name.get_call()), clrf=True)
    execute_application.append(Call('print', args=['f"Hello, {name}!"']))
    execute_application.add_return(0)
    main_class.append(execute_application)

    # main()
    main_func = Function('main')
    main_func.append(Expression(main_class.name.lower()).assign(f"{main_class.name}(sys.argv[1:])"), clrf=True)
    exp = Expression('exit_code').assign(execute_application.get_call(main_class.name.lower()))
    main_func.append(exp, clrf=True)
    main_func.append(Call('sys.exit', [exp.use()]))
    source.body.append(main_func)

    # if __name__ == "__main__"
    check_main = Statement('if', Expression('__name__') == '"__main__"')
    check_main.append(main_func.get_call())
    source.body.append(check_main)

    source.write()


if __name__ == '__main__':
    start = time.time()
    main()
    print('Generated in', round(time.time() - start, 5))
