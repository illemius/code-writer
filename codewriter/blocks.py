import inspect
import typing

from codewriter.utils import get_type_name
from codewriter.writer import CodeObject, SourceWriter

PASS = 'pass'
RETURN_ANNOTATION = '$RETURN'


class Comment(CodeObject):
    def __init__(self, *content, sep=' '):
        self.content = sep.join(map(str, content))

    def write(self, writer: SourceWriter):
        writer.new_line()
        writer.writeln('#', self.content)


class Docstring(CodeObject):
    def __init__(self, *lines, as_variable=False):
        self.lines = list(filter(lambda item: not item.strip().startswith(':'), lines))
        self.params = list(filter(lambda item: item.strip().startswith(':'), lines))
        self.as_variable = as_variable

    @property
    def docstring(self):
        return '\n'.join(map(str, self.lines))

    def write_docstring(self, writer: SourceWriter):
        writer.new_line()
        if self.lines or self.params:
            if self.as_variable:
                writer.write('__doc__ = ')
            writer.write('"""')

            if len(self.lines) == 1 and not self.params:
                writer.write_object(self.lines[0], new_line=True)
            else:
                writer.writeln()
                for line in self.lines:
                    writer.write_object(line, new_line=True)

            if self.params:
                if self.lines:
                    writer.new_line(True)
                for param in self.params:
                    writer.writeln(param)

            writer.writeln('"""')

    def write(self, writer: SourceWriter):
        self.write_docstring(writer)

    def add_line(self, *content, sep=' '):
        self.lines.append(sep.join(map(str, content)))
        return self

    def add_empty_line(self):
        self.lines.append(CLRF())
        return self

    def add_docstring(self, title, name=None, description=None):
        string = ':' + title
        if name:
            string += ' ' + name
        string += ': '
        if description:
            string += description
        self.params.append(string)
        return self

    def add_param(self, name, description, type_annotation=None):
        self.add_docstring('param', name, description)
        if type_annotation:
            self.add_type(name, type_annotation)
        return self

    def add_type(self, name, annotation):
        self.add_docstring('type', name, get_type_name(annotation))
        return self

    def add_return(self, description, type_annotation=None):
        self.add_docstring('return', description=description)
        if type_annotation:
            self.add_rtype(type_annotation)
        return self

    def add_rtype(self, annotation):
        self.add_docstring('rtype', description=get_type_name(annotation))
        return self

    def add_raise(self, exception):
        self.add_docstring('raise', description=get_type_name(exception))

    def not_empty(self):
        return len(self.lines) > 0 or len(self.params) > 0

    def __bool__(self):
        return self.not_empty()


class Block(CodeObject):
    def __init__(self, body=None, split=False, pass_if_empty=False):
        self._body = []
        self._split = split
        self._pass_if_empty = pass_if_empty

        if body is not None:
            self.extend(body)

    def append(self, code_object, clrf=False):
        self._body.append(code_object)
        if clrf:
            self._body.append(CLRF())

    def extend(self, iterable: typing.Iterable):
        for item in iterable:
            self.append(item)

    def get_body(self) -> typing.List[typing.Union[CodeObject, str]]:
        return self._body

    def write_body(self, writer: SourceWriter):
        indent = writer.indent_level

        for code_object in self._body:
            if self._split:
                writer.new_line(True)
            writer.write_object(code_object)
            if indent > writer.indent_level:
                writer.indent_level = indent
        if not self._body and self._pass_if_empty:
            writer.writeln(PASS)

        writer.end_block()

    def write(self, writer: SourceWriter):
        writer.new_line(True)
        return self.write_body(writer)

    def __len__(self):
        return len(self._body)


class Import(CodeObject):
    def __init__(self, *names):
        self._target = names
        self._from_module = None
        self._as_name = None

    def _prepare(self, data):
        return [str(item) if item is not None else '' for item in data]

    def get_target(self):
        return ', '.join(self._prepare(self._target))

    def from_module(self, *name):
        self._from_module = name
        return self

    def get_from_module(self) -> str:
        return '.'.join(self._prepare(self._from_module))

    def as_name(self, name):
        self._as_name = name
        return self

    def get_as_name(self):
        return self._as_name

    def write(self, writer: SourceWriter):
        if self._from_module:
            writer.write('from ')
            writer.write(self.get_from_module())
            writer.write(' ')

        writer.write('import', self.get_target())
        if self._as_name:
            writer.writeln(' as', self.get_as_name())
        else:
            writer.writeln()


class Return(CodeObject):
    def __init__(self, *items):
        self.items = items

    def write(self, writer: SourceWriter):
        writer.new_line()
        writer.write('return ')
        for item in self.items:
            writer.write_object(item, new_line=False)
            if item is not self.items[-1]:
                writer.write(', ')


class Function(Block):
    def __init__(self, name, args=None, kwargs=None, type_annotations=None, docstring=None, decorators=None,
                 async=False, body=None):
        if decorators is None:
            decorators = []
        if type_annotations is None:
            type_annotations = {}
        if kwargs is None:
            kwargs = {}
        if args is None:
            args = []

        generate_doc = docstring is True

        if not isinstance(docstring, Docstring):
            if isinstance(docstring, str):
                docs = docstring.splitlines()
            elif isinstance(docstring, list):
                docs = docstring
            else:
                docs = []
            docstring = Docstring(*docs)

        self.name = name
        self.args = args
        self.kwargs = kwargs
        self.type_annotations = type_annotations
        self.docstring = docstring
        self.decorators = decorators
        self.async = async

        if generate_doc:
            self.generate_docstring()
        super(Function, self).__init__(body, pass_if_empty=True)

    def generate_docstring(self):
        for arg in self.args:
            if arg in ['self', 'cls', 'mcs']:
                continue

            self.docstring.add_param(arg, '')
            if arg in self.type_annotations:
                self.docstring.add_type(arg, self.type_annotations[arg])
        for kwarg in self.kwargs:
            self.docstring.add_param(kwarg, '')
            if kwarg in self.type_annotations:
                self.docstring.add_type(kwarg, self.type_annotations[kwarg])
        if RETURN_ANNOTATION in self.type_annotations:
            self.docstring.add_return('', self.type_annotations[RETURN_ANNOTATION])

    def write_decorators(self, writer: SourceWriter):
        if self.decorators:
            for decorator in self.decorators:
                writer.writeln(f"@{decorator}")

    def write_def(self, writer: SourceWriter):
        if self.async:
            writer.write('async ')
        writer.write('def', self.name)

    def write_args(self, writer: SourceWriter):
        writer.write('(')

        if self.args:
            for arg in self.args:
                writer.write(arg)
                if arg in self.type_annotations:
                    writer.write(':', get_type_name(self.type_annotations[arg]))
                if arg is not self.args[-1] or self.kwargs:
                    writer.write(', ')

        if self.kwargs:
            for index, (kwarg, value) in enumerate(self.kwargs.items(), start=1):
                writer.write(kwarg)
                if kwarg in self.type_annotations:
                    writer.write(':', get_type_name(self.type_annotations[kwarg]))
                    writer.write(' = ')
                else:
                    writer.write('=')
                writer.write_object(value, new_line=False)

                if index != len(self.kwargs):
                    writer.write(', ')

        writer.write(')')

    def write_function(self, writer: SourceWriter):
        self.write_def(writer)
        self.write_args(writer)

        if RETURN_ANNOTATION in self.type_annotations:
            writer.write(' ->', get_type_name(self.type_annotations[RETURN_ANNOTATION]))

        writer.writeln(':')

    def write_docstring(self, writer: SourceWriter):
        if self.docstring.not_empty():
            self.docstring.write(writer)

    def write(self, writer: SourceWriter):
        writer.new_line()
        self.write_decorators(writer)
        self.write_function(writer)
        self.write_docstring(writer)
        self.write_body(writer)

        writer.end_block()

    def get_call(self, args=None, kwargs=None) -> str:
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}
        string = self.name + '('

        if args:
            for arg in args:
                string += str(arg)
                if arg is not args[-1] or kwargs:
                    string += ', '

        if kwargs:
            for index, (kwarg, value) in enumerate(kwargs.items(), start=1):
                string += f"{kwarg}={value}"
                if index != len(kwargs):
                    string += ', '

        string += ')'

        return string

    def add_return(self, *items):
        self.append(Return(*items))
        return self


class Class(Block):
    def __init__(self, name, bases=None, metaclass=None, decorators=None, docstring=None, body=None):
        if bases is None:
            bases = []
        if decorators is None:
            decorators = []

        if not isinstance(docstring, Docstring):
            if isinstance(docstring, str):
                docs = docstring.splitlines()
            elif isinstance(docstring, list):
                docs = docstring
            else:
                docs = []
            docstring = Docstring(*docs)

        self.name = name
        self.metaclass = metaclass
        self.bases = bases
        self.docstring = docstring
        self.decorators = decorators

        super(Class, self).__init__(body, pass_if_empty=True)

    def write_decorators(self, writer: SourceWriter):
        if self.decorators:
            for decorator in self.decorators:
                writer.writeln(f"@{decorator}")

    def write_class(self, writer: SourceWriter):
        writer.write('class', self.name)
        if self.bases or self.metaclass:
            writer.write('(')
            if self.bases:
                for base in self.bases:
                    writer.write(base)
                    if base is not self.bases[-1] or self.metaclass:
                        writer.write(', ')
            if self.metaclass:
                writer.write('metaclass=', self.metaclass, sep='')
            writer.write(')')
        writer.writeln(':')

    def write_docstring(self, writer: SourceWriter):
        if self.docstring.not_empty():
            self.docstring.write(writer)

    def write(self, writer: SourceWriter):
        writer.new_line()
        self.write_decorators(writer)
        self.write_class(writer)
        self.write_docstring(writer)
        self.write_body(writer)
        writer.end_block()


class Method(Function):
    def __init__(self, name, args=None, kwargs=None, type_annotations=None, docstring=None, decorators=None,
                 async=False, body=None, instance='self'):
        if args is None:
            args = []

        args.insert(0, instance)

        super(Method, self).__init__(name, args=args, kwargs=kwargs, type_annotations=type_annotations,
                                     docstring=docstring, decorators=decorators, async=async, body=body)

    def get_call(self, instance='self', args=None, kwargs=None):
        string = super(Method, self).get_call(args, kwargs)

        if instance:
            return f"{instance}.{string}"
        return ''


class ClassMethod(Method):
    def __init__(self, name, args=None, kwargs=None, type_annotations=None, docstring=None, decorators=None,
                 async=False, body=None):
        if decorators is None:
            decorators = []
        decorators.insert(0, 'classmethod')

        super(ClassMethod, self).__init__(name, args=args, kwargs=kwargs, type_annotations=type_annotations,
                                          docstring=docstring, decorators=decorators, async=async, body=body,
                                          instance='cls')

    def get_call(self, instance='cls', args=None, kwargs=None):
        if instance is None:
            instance = self.name
        return super(ClassMethod, self).get_call(instance, args, kwargs)


class Statement(Block):
    def __init__(self, name, params=None, as_name=None, async=False, body=None):
        self.name = name
        self.params = params
        self.as_name = as_name
        self.async = async

        super(Statement, self).__init__(body, pass_if_empty=True)

    def write_statement(self, writer: SourceWriter):
        if self.async:
            writer.write('async ')
        writer.write(self.name)
        if self.params:
            writer.write(' ')
            if isinstance(self.params, CodeObject):
                self.params.write(writer)
            else:
                writer.write(self.params)
        if self.as_name and self.name == 'with':
            writer.write(' as', self.as_name)
        writer.writeln(':')

    def write(self, writer: SourceWriter):
        writer.new_line()
        self.write_statement(writer)
        self.write_body(writer)
        writer.end_block()


class Expression(CodeObject):
    def __init__(self, value, is_type=False):
        if is_type:
            value = get_type_name(value)
        self.value = value

        self.log = []

    def end_line(self):
        self.log.append('\n')
        return self

    def operand(self, symbol, other):
        value = other if isinstance(other, CodeObject) else str(other)
        self.log.extend([symbol, value])
        return self

    def write(self, writer: SourceWriter):
        writer.write_object(self.value, new_line=False)
        for item in self.log:
            writer.write(' ')
            writer.write_object(item, False)

    def assign(self, other):
        return self.operand('=', other)

    def use(self):
        return self.__class__(self.value)

    def contains_in(self, item):
        return self.operand('in', item)

    def __eq__(self, other):
        return self.operand('==', other)

    def __add__(self, other):
        return self.operand('+', other)

    def __iadd__(self, other):
        return self.operand('+=', other)

    def __mul__(self, other):
        return self.operand('*', other)

    def __imul__(self, other):
        return self.operand('*=', other)

    def __or__(self, other):
        return self.operand('or', other)

    def __and__(self, other):
        return self.operand('and', other)

    def __lt__(self, other):
        return self.operand('<', other)

    def __gt__(self, other):
        return self.operand('>', other)

    def __le__(self, other):
        return self.operand('<=', other)

    def __ge__(self, other):
        return self.operand('>=', other)

    def __ne__(self, other):
        return self.operand('!=', other)


class Pairs(Block):
    def __init__(self, start, end, body):
        self.start = start
        self.end = end
        super(Pairs, self).__init__(body)

    def write_body(self, writer: SourceWriter):
        pass


class Call(CodeObject):
    def __init__(self, name, args=None, kwargs=None, await=False):
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}

        self.name = name
        self.args = args
        self.kwargs = kwargs
        self.await = await

    def write(self, writer: SourceWriter):
        if self.await:
            writer.write('await ')
        writer.write_object(self.name, new_line=False)
        writer.write('(')

        for arg in self.args:
            writer.write_object(arg, new_line=False)
            if arg is not self.args[-1] or self.kwargs:
                writer.write(', ')

        for index, (kwarg, value) in enumerate(self.kwargs.items(), start=1):
            writer.write(kwarg, '=', sep='')
            writer.write_object(value)
            if index != len(self.kwargs):
                writer.write(', ')

        writer.write(')')


class Ternary(CodeObject):
    def __init__(self, condition, on_true, on_false):
        self.condition = condition
        self.on_true = on_true
        self.on_false = on_false

    def write(self, writer: SourceWriter):
        writer.write_object(self.on_true, new_line=False)
        writer.write(' if ')
        writer.write_object(self.condition, new_line=False)
        writer.write(' else ')
        writer.write_object(self.on_false, new_line=False)


class CLRF(CodeObject):
    def write(self, writer: SourceWriter):
        writer.writeln()


class SourceCode(CodeObject):
    def __init__(self, target):
        self.target = target

    def calc_spaces(self, line):
        for pos, symbol in enumerate(line):
            if symbol != ' ':
                return pos
        return -1

    def get_lines(self):
        lines = inspect.getsourcelines(self.target)[0]
        indent = self.calc_spaces(lines[0])
        for line in lines:
            yield line[indent:].rstrip()

    def write(self, writer: SourceWriter):
        for line in self.get_lines():
            writer.writeln(line, no_auto_indent=True)
