import io
import typing

DEFAULT_FILE_MODE = 'w+'
DEFAULT_SEPARATOR = ' '
DEFAULT_INDENT_SIZE = 4


class SourceWriter:
    """This class should be used to build .py source files"""

    def __init__(self, stream: typing.Union[io.BytesIO, str], indent=DEFAULT_INDENT_SIZE):
        self.stream = open(stream, DEFAULT_FILE_MODE) if isinstance(stream, str) else stream
        self.indent_size = indent

        self._indent_level = 0
        self.on_new_line = False

        # Was a new line added automatically before? If so, avoid it
        self.auto_added_line = False

        # Can be not real value.
        self.line_number = 0

    @property
    def indent_level(self):
        return self._indent_level

    @indent_level.setter
    def indent_level(self, value):
        self._indent_level = max(0, value)

    def indent(self):
        """Indents the current source codewriter line by the current indentation level"""
        self.write(' ' * (self._indent_level * self.indent_size))

    def write(self, *strings, sep=DEFAULT_SEPARATOR):
        """Writes a string into the source codewriter, applying indentation if required"""
        string = sep.join(map(str, strings))
        if self.on_new_line:
            self.on_new_line = False  # We're not on a new line anymore
            if string.strip():
                # If the string was not empty, indent; Else it probably was a new line
                self.indent()

        self.stream.write(string)

    def writeln(self, *strings, sep=DEFAULT_SEPARATOR, no_auto_indent=False):
        """Writes a string into the source codewriter _and_ appends a new line, applying indentation if required"""
        string = sep.join(map(str, strings))
        self.write(string + '\n')

        self.on_new_line = True

        # If we're writing a block, increment indent for the next time
        if not no_auto_indent and string and string[-1] == ':':
            self.indent_level += 1

        # Clear state after the user adds a new line
        self.auto_added_line = False
        self.line_number += 1

    def new_line(self, force=False):
        if self.line_number == 0:
            return
        if force or not self.on_new_line:
            self.writeln()

    def write_object(self, obj, new_line=True):
        if isinstance(obj, CodeObject):
            obj.write(self)
        elif new_line:
            self.writeln(obj)
        else:
            self.write(obj)

    def end_block(self):
        """Ends an indentation block, leaving an empty line afterwards"""
        self.indent_level -= 1

        # If we did not add a new line automatically yet, now it's the time!
        if not self.auto_added_line:
            self.writeln()
            self.auto_added_line = True

    def close(self):
        self.stream.close()

    def __str__(self):
        self.stream.seek(0)
        return self.stream.read()

    def __len__(self):
        return len(str(self))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stream.close()


class CodeObject:
    def write(self, writer: SourceWriter):
        raise NotImplementedError
