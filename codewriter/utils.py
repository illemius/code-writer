import sys
from typing import GenericMeta


def get_type_name(obj):
    if isinstance(obj, str):
        return obj
    elif isinstance(obj, GenericMeta):
        return obj
    elif obj.__class__ is type:
        return obj.__name__
    return type(obj).__name__


def upper_case(text):
    """
    Transform text to UPPER_CASE

    :param text:
    :return:
    """
    if text.isupper():
        return text
    result = ''
    for pos, symbol in enumerate(text):
        if symbol.isupper() and pos > 0:
            result += '_' + symbol
        else:
            result += symbol.upper()
    return result


def lower_case(text):
    if text.islower():
        return text
    return upper_case(text).lower()


def log(part, name, *lines, out=sys.stderr):
    for index, line in enumerate(lines):
        if index == 0:
            print(f"[{part}] {name.upper()}:", line, file=out)
        else:
            print('\t', line, file=out)


def info(part, *lines, out=sys.stderr):
    log(part, 'info', *lines, out=out)


def warning(part, *lines, out=sys.stderr):
    log(part, 'warning', *lines, out=out)
