import os

import requests
from lxml import html

session = requests.Session()


def get_content(url):
    # raw_page = session.get(url).text
    with open('/home/jrootjunior/Telegram Bot API.html', 'r') as f:
        raw_page = f.read()
    return html.fromstring(raw_page, url)


def parse(url, page):
    classes = []
    methods = []

    for br in page.xpath("*//br"):
        br.tail = "\n" + br.tail if br.tail else "\n"

    headers = page.xpath('//h4')

    for header in headers:
        title: str = header.text_content()
        if ' ' in title.strip():
            continue
        part_url = url + header.find('a').values()[-1]

        content = []
        table = []

        elem = header.getnext()
        while elem is not None and elem.tag != 'h4':
            if elem.tag == 'p':
                content.append(elem.text_content().strip())
            elif elem.tag == 'blockquote':
                content.append(elem.text_content().strip())
            elif elem.tag == 'table':
                for line in elem.xpath('.//tr'):
                    row = []
                    for item in line.xpath('.//td'):
                        row.append(item.text_content())
                    table.append(row)
            elem = elem.getnext()

        table = fmt_table(table)
        part = {
            'title': title,
            'url': part_url,
            'description': content,
            'annotations': table
        }
        if title[0].isupper():
            classes.append(part)
        else:
            methods.append(part)

    return classes, methods


def fmt_table(tab: list):
    if not tab:
        return []
    title = list(map(str.lower, tab.pop(0)))
    return [dict(zip(title, row)) for row in tab]
