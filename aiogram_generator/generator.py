import os
import re

from codewriter.blocks import *
from codewriter.source import Source
from codewriter.utils import info, lower_case, upper_case, warning

BUILTIN_TYPES = ['String', 'Integer', 'Float', 'Boolean', 'InputFile']
RE_FLAGS = re.IGNORECASE

RETURN_PATTERNS = [
    re.compile(r'(?P<type>Array of [a-z]+) objects', flags=RE_FLAGS),
    re.compile(r'a (?P<type>[a-z]+) object', flags=RE_FLAGS),
    re.compile(r'Returns (?P<type>[a-z]+) on success', flags=RE_FLAGS),
    re.compile(r'(?P<type>[a-z]+) on success', flags=RE_FLAGS),
    re.compile(r'(?P<type>[a-z]+) is returned, otherwise (?P<other>[a-zA-Z]+) is returned', flags=RE_FLAGS),
    re.compile(r'returns the edited (?P<type>[a-z]+), otherwise returns (?P<other>[a-zA-Z]+)', flags=RE_FLAGS),
    re.compile(r'(?P<type>[a-z]+) is returned', flags=RE_FLAGS),
    re.compile(r'Returns (?P<type>[a-z]+)', flags=RE_FLAGS),
]

READ_MORE_PATTERN = re.compile(r'(More info on[\W\w]+»)', flags=re.MULTILINE & re.IGNORECASE)


def update_params(data):
    for field in data:
        optional = False
        if 'optional' in field.get('description', '').lower():
            optional = True
        if field.get('required', '').lower() in ['optional', 'no']:
            optional = True
        field['optional'] = optional
    return data


def generate_methods(path, methods):
    source = Source(path, add_header=True)
    source.add_import('typing')
    source.add_import('types').from_module('aiogram')
    source.add_import('base').from_module('aiogram', 'types')
    source.add_import('api').from_module('aiogram.bot.base')
    source.add_import('BaseBot').from_module('aiogram', 'bot', 'base')
    source.add_import('generate_payload, prepare_arg').from_module('aiogram', 'utils', 'payload')

    bot_class = Class('Bot', bases=['BaseBot'])

    for method in methods:
        method['annotations'] = update_params(method['annotations'])
        method_obj = generate_method(method)
        bot_class.append(method_obj)

    source.body.append(bot_class)
    source.write()


def normalize_type(string):
    if not string:
        return 'typing.Any'

    lower = string.lower()
    split = lower.split()

    if split[0] == 'array':
        new_string = string[lower.index('of') + 2:].strip()
        return f"typing.List[{normalize_type(new_string)}]"
    if 'or' in split:
        split_types = string.split(' or ')
        norm_str = ', '.join(map(normalize_type, map(str.strip, split_types)))
        return f"typing.Union[{norm_str}]"
    if 'number' in lower:
        return normalize_type(string.replace('number', '').strip())
    if lower in ['true', 'false']:
        return 'base.Boolean'
    if lower == 'int':
        return 'base.Integer'
    if string not in BUILTIN_TYPES and string[0].isupper():
        return f"types.{string}"
    elif string in BUILTIN_TYPES:
        return 'base.' + string
    return 'typing.Any'


def get_returning(description):
    parts = list(filter(lambda item: 'return' in item.lower(), description.split('.')))
    if not parts:
        return 'typing.Any', ''
    sentence = '. '.join(map(str.strip, parts))
    return_type = None

    for pattern in RETURN_PATTERNS:
        temp = pattern.search(sentence)
        if temp:
            return_type = temp.group('type')
            if 'other' in temp.groupdict():
                otherwise = temp.group('other')
                return_type += f" or {otherwise}"
        if return_type:
            break

    return normalize_type(return_type), sentence + '.'


def strip_line(string):
    lines = string.strip().splitlines()
    return lines[0].strip()


def generate_method(data):
    method = Method(lower_case(data['title']), async=True)

    for line in data['description']:
        for sub_line in line.splitlines():
            description_line = ' '.join(READ_MORE_PATTERN.sub('', sub_line).split())
            method.docstring.add_line(description_line.rstrip(':'))

    exclude = None

    for field in data['annotations']:
        param = field['parameters']
        param_type = normalize_type(field['type'])
        if field.get('optional'):
            param_type = f"typing.Union[{param_type}, None]"
            method.kwargs[param] = None
        else:
            method.args.append(param)

        method.type_annotations[param] = param_type
        description_line = ' '.join(READ_MORE_PATTERN.sub('', field['description']).split())
        method.docstring.add_param(param, strip_line(description_line), f":obj:`{param_type}`")
        if 'types.' in param_type:
            method.append(Expression(param).assign(f"prepare_arg({param})"), clrf=True)
        elif 'date' in param:
            method.append(Expression(param).assign(f"prepare_arg({param})"), clrf=True)
        elif 'InputFile' in param_type:
            exclude = param

    method.docstring.add_empty_line()
    method.docstring.add_line('Source', data['url'])

    if not exclude:
        method.append(Expression('payload').assign("generate_payload(**locals())"), clrf=True)
        method.append(f"result = await self.request(api.Methods.{upper_case(data['title'])}, payload)")
    else:
        method.append(Expression('payload').assign(f"generate_payload(**locals(), exclude=['{exclude}'])"), clrf=True)
        method.append(f"result = await self.send_file('{exclude}', "
                      f"api.Methods.{upper_case(data['title'])}, {exclude}, payload)")

    method.append(CLRF())
    return_type, return_description = get_returning(method.docstring.docstring)
    method.docstring.add_return(return_description, f":obj:`{return_type}`")

    if 'List' in return_type:
        temp_type = return_type.replace('typing.List[', '').replace(']', '')
        min_type = temp_type.rpartition('.')[-1].lower()
        method.add_return(f"[{temp_type}(**{min_type}) for {min_type} in result]")
    elif return_type.startswith('base.'):
        method.add_return('result')
    else:
        if 'Union' in return_type:
            temp_type = return_type.replace('typing.Union[', '').replace(']', '').split(', ')
            if 'base.Boolean' in temp_type:
                temp_type.remove('base.Boolean')
                method.append(Statement('if', f"isinstance(result, bool)", body=[
                    'return result'
                ]))
            temp_type = temp_type.pop()
        else:
            temp_type = return_type
        method.add_return(f"{temp_type}(**result)")

    method.append(CLRF())
    method.type_annotations[RETURN_ANNOTATION] = return_type

    if return_type == 'typing.Any':
        warning('Methods',
                f"Cannot resolve returns type type in '{data['title']}'",
                f"In code: '{method.name}'",
                f"Sentence: '{return_description}'",
                "Used default value: 'typing.Any'")

    return method


def generate_types(types_path, classes):
    for data_type in classes:
        file_path = os.path.join(types_path, lower_case(data_type['title']) + '.py')
        info('Generator', 'Generate class', f"Path: {file_path}")
        src = Source(file_path)
        src.add_import('base').from_module('.')
        src.add_import('fields').from_module('.')
        src.add_import('typing')

        klass = generate_class(data_type, src)
        src.body.append(klass)

        src.write()


PROP_NAME_ALIASES = {
    'type': '{class_name}_type',
    'from': 'from_user'
}


def generate_class(data, src: Source):
    klass = Class(data['title'], bases=['base.TelegramObject'])
    for line in data['description']:
        for sub_line in line.splitlines():
            description_line = ' '.join(READ_MORE_PATTERN.sub('', sub_line).split())
            klass.docstring.add_line(description_line.rstrip(':'))
    klass.docstring.add_empty_line()
    klass.docstring.add_line(data['url'])

    required_types = []

    for prop in data.get('annotations', {}):
        prop_name = prop['field']
        prop_kwargs = []
        if prop_name in PROP_NAME_ALIASES:
            prop_kwargs.append(f"alias='{prop_name}'")
            prop_name = PROP_NAME_ALIASES[prop_name].format(class_name=lower_case(data['title']))

        prop_type = normalize_type(prop['type'])
        if 'types.' in prop_type:
            prop_type = prop_type.replace('types.', '')
            cleaned_type = prop_type.replace('typing.List[', '').replace(']', '')
            if prop_type not in required_types and data['title'] != prop_type:
                required_types.append(cleaned_type)
            prop_kwargs.append(f"base={cleaned_type}" if data['title'] != prop_type else f"base='{cleaned_type}'")
        if data['title'] == prop_type:
            prop_type = f"'{prop_type}'"
        for def_type in BUILTIN_TYPES:
            prop_type = prop_type.replace(def_type, f"base.{def_type}")
        if 'typing.List' in prop_type:
            prop_field_type = 'ListField'
        else:
            prop_field_type = 'Field'

        line = f"{prop_name}: {prop_type} = fields.{prop_field_type}({', '.join(prop_kwargs)})"
        klass.append(line)
    for imp in required_types:
        src.add_import(imp).from_module(f".{lower_case(imp)}")
    return klass
