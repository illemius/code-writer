import json
import os

from aiogram_generator.generator import generate_types, generate_methods
from aiogram_generator.parser import get_content, parse
from codewriter.utils import info

TELEGRAM_API_URL = 'https://core.telegram.org/bots/api'

OUT_PATH = os.path.abspath('./out')


def write(*path, name, text=None, data=None):
    """

    :param path:
    :param name:
    :param text:
    :param data:
    :return:
    """
    path = os.path.join(*path)
    os.makedirs(path, exist_ok=True)

    info('Dumps', 'Saved', f"Path: '{os.path.join(path, name)}'")
    with open(os.path.join(path, name), 'w') as file:
        if text:
            file.write(text)
        elif data:
            json.dump(data, file, indent=4, ensure_ascii=False)


def main():
    info('Generator', 'Getting latest API description...')
    page = get_content(TELEGRAM_API_URL)

    info('Generator', 'Generating tree...')
    classes, methods = parse(TELEGRAM_API_URL, page)

    # info('Generator', 'Write dumps.')
    # write(OUT_PATH, 'element_tree', name='_classes.json', data=classes)
    # write(OUT_PATH, 'element_tree', name='_methods.json', data=methods)

    bot_path = os.path.join(OUT_PATH, 'bot.py')
    info('Generator', 'Generate main bot class', f"Path: {bot_path}")
    generate_methods(bot_path, methods)

    # types_path = os.path.join(OUT_PATH, 'types')
    # generate_types(types_path, classes)


if __name__ == '__main__':
    main()
